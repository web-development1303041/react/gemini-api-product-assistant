import { useState } from 'react';
import ReactMarkdown from 'react-markdown'

const App = () => {
  const [value, setValue] = useState("");
  const [error, setError] = useState("");
  const [chatHistory, setChatHistory] = useState([]);

  const surpriseOptions = [
    'Give me a brief description of AeroTern.',
    'Why is the IMU device named AeroTern?',
    'Tell me more about AeroTern.',
    'What are some application examples for AeroTern?',
    'What does AeroTern do better than other IMUs?',
    'What is the price range of AeroTern?'
  ];

  const surprise = () => {
    const randomValue = surpriseOptions[Math.floor(Math.random() * surpriseOptions.length)];
    setValue(randomValue);
  };

  const getResponse = async () => {
    if (!value) {
      setError("Error! Please ask a question!");
      return;
    }
    try {
      const options = {
        method: 'POST',
        body: JSON.stringify({
          history: chatHistory,
          message: value
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      };
      const response = await fetch('http://localhost:8000/gemini', options);
      const data = await response.text();
      console.log(data);

      setChatHistory(oldChatHistory => [
        ...oldChatHistory,
        { role: "user", parts: value },
        { role: "model", parts: data }
      ]);
      setValue("");

    } catch (error) {
      console.error(error);
      setError("Something went wrong! Please try again later.");
    }
  };

  const clear = () => {
    setValue("");
    setError("");
    setChatHistory([]);
  };

  return (
    <div className="app">
      <img src="/images/center.jpg" alt="center-img" className="center-img" />
      <p>
        Welcome! This is Ania, your Product AI Assistant.
        <button className="surprise" onClick={surprise} disabled={!chatHistory}>Get to know the product!</button>
      </p>

      <div className="input-container">
        <input
          value={value}
          placeholder="Ask me a question..."
          onChange={(e) => setValue(e.target.value)}
        />
        {!error && <button onClick={getResponse}>Ask me</button>}
        {error && <button onClick={clear}>Clear</button>}
      </div>

      {error && <p>{error}</p>}

      <div className="search-result">
        {chatHistory.map((chatItem, _index) => (
          <div key={_index}>
            {chatItem.role === "user" ? (
              <p className="answer"><strong>Me:</strong><ReactMarkdown>{chatItem.parts}</ReactMarkdown></p>
            ) : (
              <p className="answer"><strong>Ania:</strong><ReactMarkdown>{chatItem.parts}</ReactMarkdown></p>
            )}

          </div>
        ))}
      </div>

    </div>
  );
};

export default App;