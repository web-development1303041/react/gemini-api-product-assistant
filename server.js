const PORT = 8000;
const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
app.use(express.json());
require('dotenv').config();

const { GoogleGenerativeAI } = require('@google/generative-ai');

const genAI = new GoogleGenerativeAI(process.env.GOOGLE_GEN_AI_KEY);

app.post('/gemini', async (req, res) => {
    console.log(req.body.history)
    console.log(req.body.message)
    try {
        const model = genAI.getGenerativeModel({
            model: "gemini-1.5-flash",
            systemInstruction: "You're a persuasive sales representative named Ania promoting an IMU device called AeroTern. Your only job is to promote its high precision, low power consumption, and low cost. The IMU is designed for aviation related applications, but can also be used for average applications. The name of the device AeroTern is made of two words, Aerodynamics, and Arctic Tern, the latter being known for its navigational skills. More information about the product versions, datasheets and prices are posted in the official website for the buyers review. The prices starts from 3.04 euros per reel unit, or 11 euros for a soldered unit. The prices are lower when buying in bulk."
        });

        // Ensure the history has the correct structure
        const history = req.body.history.map(item => ({
            role: item.role,
            parts: Array.isArray(item.parts) ? item.parts.map(part => ({ text: part })) : [{ text: item.parts }]
        }));

        const chat = await model.startChat({
            history: history,
            generationConfig: {
                maxOutputTokens: 300
            }
        });
        const msg = req.body.message;

        const result = await chat.sendMessage(msg);
        const response = await result.response;
        const text = response.text()
        res.send(text);

    } catch (error) {
        console.error('Error occurred:', error);
        res.status(500).send({
            message: "Something went wrong! Please try again later.",
            error: error.message,
            status: error.status,
            statusText: error.statusText
        });
    }
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));