# NavAeroTern AI Product Assistant

This project is a React application that integrates with the Gemini API to act as an AI product assistant for an imaginary Inertial Measurement Unit (IMU) device called NavAeroTern. The AI assistant is designed to behave like a friendly sales representative, promoting the device's high performance, low power consumption, and low cost.

Demo video of the UI posted in: https://aerotern-ania.blogspot.com/2024/07/AniaAIPreview.html

## Features

- **Interactive Q&A:** Users can ask questions about NavAeroTern and receive detailed responses from the AI.
- **Surprise Me:** Provides random example questions to engage users and demonstrate the AI's capabilities.
- **Chat History:** Maintains a conversation history for a seamless and interactive experience.

## How It Works

### Frontend (App.js)

The frontend of the application is built using React. Here's a brief overview of the key components and functionalities:

1. **State Management:**
   - `value`: Holds the current input value from the user.
   - `error`: Stores any error messages.
   - `chatHistory`: Keeps track of the conversation history between the user and the AI.

2. **Get to know the product! Functionality:**
   - The `surprise` function selects a random question from a predefined list and sets it as the current input value.

3. **Fetching AI Responses:**
   - The `getResponse` function sends the user's question along with the chat history to the backend server.
   - If the response is successful, it updates the chat history with the AI's response. If an error occurs, it sets an error message.

4. **Clear Functionality:**
   - The `clear` function resets the input value, error message, and chat history.

5. **Rendering:**
   - The component renders an input field for the user's question, a "Surprise me" button, and a list of conversation history items.

### Backend (server.js)

The backend of the application is built using Express.js. It handles requests from the frontend and communicates with the Gemini API. Here’s an overview of its functionality:

1. **Setup:**
   - Uses Express.js and middleware such as `cors` for Cross-Origin Resource Sharing and `express.json` for parsing JSON bodies.
   - Loads environment variables using `dotenv`.

2. **Gemini API Integration:**
   - Initializes the GoogleGenerativeAI client using the provided API key.

3. **Handling Requests:**
   - Defines a POST endpoint `/gemini` that receives chat history and user messages from the frontend.
   - Maps the chat history to the format required by the Gemini API.
   - Starts a chat with the AI model, sends the user's message, and gets a response.
   - Sends the AI's response back to the frontend.

4. **Error Handling:**
   - Catches any errors that occur during the API call and sends an error message back to the frontend.

### Running the Project

#### Prerequisites

- Node.js
- npm (Node package manager)
- A Google Generative AI API key

#### Installation Steps

1. Clone the repository:

    ```bash
    git clone https://github.com/yourusername/navaerotern-ai-assistant.git
    cd navaerotern-ai-assistant
    ```

2. Install dependencies:

    ```bash
    npm install
    ```

3. Create a `.env` file in the root directory and add your Google Generative AI API key:

    ```plaintext
    GOOGLE_GEN_AI_KEY=your-google-gen-ai-key
    ```

4. Start the server:

    ```bash
    npm run start:backend
    ```

5. Start the React application:

    ```bash
    npm run start:frontend
    ```

#### Usage

Once the application is running, open it in your browser at `http://localhost:3000`. You can interact with the AI by asking questions about NavAeroTern or by using the "Surprise me" button to get random example questions. The conversation history will be displayed on the page, and you can clear it anytime using the "Clear" button.